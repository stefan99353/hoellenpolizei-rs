use crate::bot::commands::HoellenpolizeiCommand;
use crate::bot::Hoellenpolizei;
use crate::db::EnabledChannel;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::application::interaction::InteractionResponseType;
use serenity::prelude::*;

pub struct AddChannelCommand;

#[async_trait::async_trait]
impl HoellenpolizeiCommand for AddChannelCommand {
    const COMMAND: &'static str = "pozilei-add";
    const DESCRIPTION: &'static str = "Adds this channel for the bot to listen to";

    async fn process(
        hoellenpolizei: &Hoellenpolizei,
        command: &ApplicationCommandInteraction,
        ctx: &Context,
    ) -> anyhow::Result<()> {
        trace!("Getting guild information");
        let guild_id = command.guild_id.map(|id| id.to_string());
        let guild_name = command
            .guild_id
            .and_then(|id| id.name(&hoellenpolizei.cache));
        trace!("Getting channel information");
        let channel_id = command.channel_id.to_string();
        let channel_name = command.channel_id.name(&hoellenpolizei.cache).await;

        trace!("Saving channel to database");
        EnabledChannel::insert(
            channel_id,
            channel_name,
            guild_id,
            guild_name,
            &hoellenpolizei.db,
        )
        .await?;

        trace!("Responding to the command");
        command
            .create_interaction_response(&ctx.http, |response| {
                response
                    .kind(InteractionResponseType::ChannelMessageWithSource)
                    .interaction_response_data(|message| {
                        message.content("Hussa, auf gutes Gelingen!")
                    })
            })
            .await?;

        Ok(())
    }
}
