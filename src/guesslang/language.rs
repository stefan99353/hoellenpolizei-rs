#[derive(Clone, Copy, Debug, serde::Deserialize, serde::Serialize)]
pub enum Language {
    Other,
    Assembly,
    Batchfile,
    C,
    #[serde(rename = "C#")]
    CSharp,
    #[serde(rename = "C++")]
    CPlusPlus,
    Clojure,
    CMake,
    #[serde(rename = "COBOL")]
    Cobol,
    CoffeeScript,
    #[serde(rename = "CSS")]
    Css,
    #[serde(rename = "CSV")]
    Csv,
    Dart,
    #[serde(rename = "DM")]
    Dm,
    Dockerfile,
    Elixir,
    Erlang,
    Fortran,
    Go,
    Groovy,
    Haskell,
    #[serde(rename = "HTML")]
    Html,
    #[serde(rename = "INI")]
    Ini,
    Java,
    JavaScript,
    #[serde(rename = "JSON")]
    Json,
    Julia,
    Kotlin,
    Lisp,
    Lua,
    Makefile,
    Markdown,
    Matlab,
    #[serde(rename = "Objective-C")]
    ObjectiveC,
    OCaml,
    Pascal,
    Perl,
    #[serde(rename = "PHP")]
    Php,
    PowerShell,
    Prolog,
    Python,
    R,
    Ruby,
    Rust,
    Scala,
    Shell,
    #[serde(rename = "SQL")]
    Sql,
    Swift,
    #[serde(rename = "TeX")]
    Tex,
    #[serde(rename = "TOML")]
    Toml,
    TypeScript,
    Verilog,
    #[serde(rename = "Visual Basic")]
    VisualBasic,
    #[serde(rename = "XML")]
    Xml,
    #[serde(rename = "YAML")]
    Yaml,
}

impl Language {
    pub fn markdown_language(&self) -> Option<&str> {
        match self {
            Language::Other => None,
            Language::Assembly => Some("x86asm"),
            Language::Batchfile => Some("bash"),
            Language::C => Some("c"),
            Language::CSharp => Some("csharp"),
            Language::CPlusPlus => Some("cpp"),
            Language::Clojure => Some("clojure"),
            Language::CMake => Some("cmake"),
            Language::Cobol => Some("cobol"),
            Language::CoffeeScript => Some("coffeescript"),
            Language::Css => Some("css"),
            Language::Csv => Some("csv"),
            Language::Dart => Some("dart"),
            Language::Dm => Some("dm"),
            Language::Dockerfile => Some("dockerfile"),
            Language::Elixir => Some("elixir"),
            Language::Erlang => Some("erlang"),
            Language::Fortran => Some("fortran"),
            Language::Go => Some("go"),
            Language::Groovy => Some("groovy"),
            Language::Haskell => Some("haskell"),
            Language::Html => Some("html"),
            Language::Ini => Some("ini"),
            Language::Java => Some("java"),
            Language::JavaScript => Some("javascript"),
            Language::Json => Some("json"),
            Language::Julia => Some("julia"),
            Language::Kotlin => Some("kotlin"),
            Language::Lisp => Some("lisp"),
            Language::Lua => Some("lua"),
            Language::Makefile => Some("makefile"),
            Language::Markdown => Some("markdown"),
            Language::Matlab => Some("matlab"),
            Language::ObjectiveC => Some("objectivec"),
            Language::OCaml => Some("ocaml"),
            Language::Pascal => Some("pascal"),
            Language::Perl => Some("perl"),
            Language::Php => Some("php"),
            Language::PowerShell => Some("powershell"),
            Language::Prolog => Some("prolog"),
            Language::Python => Some("python"),
            Language::R => Some("r"),
            Language::Ruby => Some("ruby"),
            Language::Rust => Some("rust"),
            Language::Scala => Some("scala"),
            Language::Shell => Some("shell"),
            Language::Sql => Some("sql"),
            Language::Swift => Some("swift"),
            Language::Tex => Some("tex"),
            Language::Toml => Some("toml"),
            Language::TypeScript => Some("typescript"),
            Language::Verilog => Some("verilog"),
            Language::VisualBasic => Some("vb"),
            Language::Xml => Some("xml"),
            Language::Yaml => Some("yaml"),
        }
    }
}

impl From<String> for Language {
    fn from(value: String) -> Self {
        let value = format!("\"{value}\"");
        serde_json::from_str::<Self>(&value).unwrap_or(Self::Other)
    }
}
