use std::path::PathBuf;

#[derive(Clone, Debug, serde::Deserialize)]
pub struct Config {
    #[serde(default = "default_database")]
    pub database: PathBuf,

    #[serde(default)]
    pub discord_token: String,
    #[serde(default)]
    pub discord_guild_id: u64,

    #[serde(default)]
    pub log_level: Option<String>,
    #[serde(default = "default_log_stdout")]
    pub log_stdout: bool,
    #[serde(default)]
    pub log_file: bool,
    #[serde(default = "default_log_file_path")]
    pub log_file_path: PathBuf,
    #[serde(default = "default_log_max_file_count")]
    pub log_max_file_count: usize,
    #[serde(default = "default_log_max_file_size")]
    pub log_max_file_size: usize,
}

impl Config {
    pub fn build() -> anyhow::Result<Self> {
        let config_file = std::env::var("CONFIG_FILE").unwrap_or_else(|_| "./config".to_string());

        let builder = config::Config::builder()
            .add_source(config::File::with_name(&config_file).required(false))
            .add_source(config::Environment::with_prefix("bot"));

        let result = builder.build()?.try_deserialize()?;

        Ok(result)
    }
}

fn default_database() -> PathBuf {
    PathBuf::from("hoellenpolizei.db")
}
fn default_log_stdout() -> bool {
    true
}
fn default_log_file_path() -> PathBuf {
    PathBuf::from("logs/hoellenpolizei.log")
}
fn default_log_max_file_count() -> usize {
    5
}
fn default_log_max_file_size() -> usize {
    10000000
}
