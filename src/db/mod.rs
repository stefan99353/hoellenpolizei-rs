mod enabled_channel;

pub use enabled_channel::EnabledChannel;
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::{ConnectOptions, Pool, Sqlite};
use std::path::Path;
use tracing::log::LevelFilter;

#[instrument(
    name = "connect_and_migrate_database",
    level = "trace",
    skip_all,
    fields(path = %path.as_ref().display())
)]
pub async fn connect_and_migrate(path: impl AsRef<Path>) -> anyhow::Result<Pool<Sqlite>> {
    trace!("Building connect options");
    let mut connect_options = SqliteConnectOptions::new()
        .filename(path)
        .create_if_missing(true);

    connect_options.log_statements(LevelFilter::Trace);

    trace!("Connecting to databse");
    let pool = SqlitePoolOptions::new()
        .max_connections(10)
        .connect_with(connect_options)
        .await?;

    trace!("Migrating database");
    sqlx::migrate!().run(&pool).await?;

    Ok(pool)
}
