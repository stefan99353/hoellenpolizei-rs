mod commands;
mod messages;

use crate::config::Config;
use crate::logging::LogReloadHandle;
use once_cell::sync::OnceCell;
use serenity::cache::Settings;
use serenity::client::Cache;
use serenity::http::Http;
use serenity::model::application::interaction::Interaction;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::model::id::{ChannelId, GuildId, UserId};
use serenity::prelude::*;
use serenity::Client;
use sqlx::{Pool, Sqlite};

pub struct Hoellenpolizei {
    pub bot_id: OnceCell<UserId>,
    pub config: Config,
    pub db: Pool<Sqlite>,
    pub cache: Cache,
}

impl Hoellenpolizei {
    pub async fn start(
        config: Config,
        db: Pool<Sqlite>,
        _log_reload: LogReloadHandle,
    ) -> anyhow::Result<()> {
        trace!("Creating cache");
        let mut cache_settings = Settings::new();
        cache_settings.max_messages(256);
        let cache = Cache::new_with_settings(cache_settings);

        let intents = GatewayIntents::GUILD_MESSAGES
            | GatewayIntents::DIRECT_MESSAGES
            | GatewayIntents::MESSAGE_CONTENT;

        trace!("Building client");
        let hoellenpolizei = Hoellenpolizei {
            bot_id: OnceCell::default(),
            config: config.clone(),
            db,
            cache,
        };

        let mut client = Client::builder(&config.discord_token, intents)
            .event_handler(hoellenpolizei)
            .await?;

        debug!("Starting bot client");
        client.start().await?;

        Ok(())
    }
}

#[async_trait::async_trait]
impl EventHandler for Hoellenpolizei {
    // Interaction (commands)
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        trace!("Received an interaction");

        if let Interaction::ApplicationCommand(command) = interaction {
            commands::handle_command(self, command, ctx).await;
        }
    }

    // Messages
    async fn message(&self, ctx: Context, msg: Message) {
        trace!("Received a message");

        messages::handle_message(self, msg, ctx).await
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        trace!("Saving bot user id");
        self.bot_id
            .set(ready.user.id)
            .expect("Bot user id already set");

        trace!("Register commands");
        let register_result = GuildId::set_application_commands(
            &GuildId(self.config.discord_guild_id),
            &ctx.http,
            commands::register_commands,
        )
        .await;

        if let Err(err) = register_result {
            error!("Error during command registration: {err}");
        }
    }
}

pub async fn channel_error(
    channel: ChannelId,
    http: impl AsRef<Http>,
    err: impl std::fmt::Display,
) {
    error!("{err}");

    trace!("Writing error to channel");
    if let Err(err) = channel.say(http, "Du alte Memme, denkst du hast Probleme!").await {
        error!("Failed to send error to channel: {err}");
    }
}
