mod add;
mod remove;

use crate::bot::{channel_error, Hoellenpolizei};
use add::AddChannelCommand;
use remove::RemoveChannelCommand;
use serenity::builder::{CreateApplicationCommand, CreateApplicationCommands};
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::application::interaction::InteractionResponseType;
use serenity::prelude::*;
use time::Instant;

pub fn register_commands(
    commands: &mut CreateApplicationCommands,
) -> &mut CreateApplicationCommands {
    commands
        .create_application_command(AddChannelCommand::register)
        .create_application_command(RemoveChannelCommand::register)
}

#[instrument(
    name = "handle_command",
    skip_all,
    fields(
        name = command.data.name,
        guild_id = ?command.guild_id,
        channel = %command.channel_id,
        user_id = %command.user.id,
        user_name = command.user.name,
    )
)]
pub async fn handle_command(
    hoellenpolizei: &Hoellenpolizei,
    command: ApplicationCommandInteraction,
    ctx: Context,
) {
    trace!("Interaction is a command");
    match command.data.name.as_str() {
        AddChannelCommand::COMMAND => {
            AddChannelCommand::handle(hoellenpolizei, &command, &ctx).await
        }
        RemoveChannelCommand::COMMAND => {
            RemoveChannelCommand::handle(hoellenpolizei, &command, &ctx).await
        }
        cmd => {
            warn!("Received unknown command `{cmd}`");
            let result = command
                .create_interaction_response(&ctx.http, |response| {
                    response
                        .kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|message| message.content("Was für'n Ding?!"))
                })
                .await;

            if let Err(err) = result {
                channel_error(command.channel_id, &ctx.http, err).await;
            }
        }
    };
}

#[async_trait::async_trait]
pub trait HoellenpolizeiCommand {
    const COMMAND: &'static str;
    const DESCRIPTION: &'static str;

    #[instrument(name = "register_command", level = "trace", skip_all, fields(command = Self::COMMAND))]
    fn register(command: &mut CreateApplicationCommand) -> &mut CreateApplicationCommand {
        trace!("Register command");
        command.name(Self::COMMAND).description(Self::DESCRIPTION)
    }

    #[instrument(name = "handle_command", skip_all, fields(command = Self::COMMAND))]
    async fn handle(
        hoellenpolizei: &Hoellenpolizei,
        command: &ApplicationCommandInteraction,
        ctx: &Context,
    ) {
        let start = Instant::now();

        debug!("Processing the command");
        let result = Self::process(hoellenpolizei, command, ctx).await;

        // Error
        if let Err(err) = result {
            channel_error(command.channel_id, &ctx.http, err).await;
        }

        let elapsed = start.elapsed().whole_milliseconds();
        info!("Finished processing the command. Took {}ms", elapsed);
    }

    async fn process(
        hoellenpolizei: &Hoellenpolizei,
        command: &ApplicationCommandInteraction,
        ctx: &Context,
    ) -> anyhow::Result<()>;
}
