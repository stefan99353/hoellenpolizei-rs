mod text;

use crate::bot::{channel_error, Hoellenpolizei};
use crate::db::EnabledChannel;
use serenity::model::channel::Message;
use serenity::prelude::*;
use time::Instant;

#[instrument(
    name = "handle_message",
    skip_all,
    fields(
        message_id = %msg.id,
        guild_id = ?msg.guild_id,
        channel = %msg.channel_id,
        author_id = %msg.author.id,
        author_name = msg.author.name,
    )
)]
pub async fn handle_message(bot: &Hoellenpolizei, msg: Message, ctx: Context) {
    let start = Instant::now();

    // Check if message is from bot
    if Some(&msg.author.id) == bot.bot_id.get() {
        trace!("Message author is this bot => Skipping message");
        return;
    }

    trace!("Checking if channel was added");
    let channel = msg.channel_id;
    match EnabledChannel::exists(channel.to_string(), &bot.db).await {
        Ok(true) => {
            trace!("Channel is added")
        }
        Ok(false) => {
            trace!("Channel is not added");
            return;
        }
        Err(err) => {
            channel_error(channel, &ctx.http, err).await;
            return;
        }
    }

    if let Err(err) = text::markup(bot, msg, &ctx).await {
        channel_error(channel, &ctx.http, err).await;
    }

    let elapsed = start.elapsed().whole_milliseconds();
    info!("Finished processing the message. Took {}ms", elapsed);
}
