use sqlx::{Pool, Sqlite};
use time::OffsetDateTime;

#[derive(Clone, Debug)]
pub struct EnabledChannel {
    pub channel_id: String,
    pub channel_name: Option<String>,
    pub guild_id: Option<String>,
    pub guild_name: Option<String>,
    pub added_at: OffsetDateTime,
}

impl EnabledChannel {
    #[instrument(
        name = "enabled_channel_exists",
        level = "trace",
        skip_all,
        fields(channel_id = channel_id)
    )]
    pub async fn exists(channel_id: String, db: &Pool<Sqlite>) -> anyhow::Result<bool> {
        trace!("Querying channels");
        let exists = sqlx::query!(
            r#"
            SELECT 
                COUNT(*) as count 
            FROM 
                enabled_channels 
            WHERE 
                channel_id = ?
            ;
            "#,
            channel_id
        )
        .fetch_one(db)
        .await?;

        Ok(exists.count > 0)
    }

    #[instrument(
        name = "insert_enabled_channel",
        level = "trace",
        skip_all,
        fields(
            channel_id = channel_id,
            channel_name = channel_name,
            guild_id = guild_id,
            guild_name = guild_name,
        )
    )]
    pub async fn insert(
        channel_id: String,
        channel_name: Option<String>,
        guild_id: Option<String>,
        guild_name: Option<String>,
        db: &Pool<Sqlite>,
    ) -> anyhow::Result<()> {
        trace!("Inserting channel");
        sqlx::query!(
            r#"
            INSERT INTO
                enabled_channels
                (
                    channel_id,
                    channel_name,
                    guild_id,
                    guild_name
                ) 
            VALUES 
                (?, ?, ?, ?)
            ;
            "#,
            channel_id,
            channel_name,
            guild_id,
            guild_name,
        )
        .execute(db)
        .await?;

        Ok(())
    }

    #[instrument(
        name = "delete_enabled_channel",
        level = "trace",
        skip_all,
        fields(
            channel_id = channel_id,
        )
    )]
    pub async fn delete(channel_id: String, db: &Pool<Sqlite>) -> anyhow::Result<()> {
        trace!("Deleting channel");
        sqlx::query!(
            r#"
            DELETE FROM
                enabled_channels 
            WHERE 
                channel_id = ?
            ;
            "#,
            channel_id,
        )
        .execute(db)
        .await?;

        Ok(())
    }
}
