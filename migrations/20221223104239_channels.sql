-- Add migration script here

CREATE TABLE enabled_channels(
    channel_id TEXT PRIMARY KEY NOT NULL,
    channel_name TEXT,
    guild_id TEXT,
    guild_name TEXT,
    added_at DATETIME NOT NULL DEFAULT current_timestamp
);