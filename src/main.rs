#[macro_use]
extern crate tracing;

mod bot;
mod config;
mod db;
mod guesslang;
mod logging;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // Env
    dotenvy::dotenv().ok();

    // Config
    let config = config::Config::build()?;

    // Logging
    let (reload_handle, _guard) = logging::init(&config)?;

    // Trace config
    trace!("{config:#?}");

    // Database
    debug!("Setting up database");
    let db = db::connect_and_migrate(&config.database).await?;

    // Start bot
    info!("Starting Höllenpolizei bot");
    bot::Hoellenpolizei::start(config, db, reload_handle).await?;

    Ok(())
}
