# Höllenpolizei

[![MSRV](https://img.shields.io/badge/MSRV-1.66-lightgray.svg)](https://blog.rust-lang.org/2022/12/15/Rust-1.66.0.html)
[![Dependency Status](https://deps.rs/repo/gitlab/stefan99353/hoellenpolizei-rs/status.svg)](https://deps.rs/repo/gitlab/stefan99353/hoellenpolizei-rs)
[![License](https://img.shields.io/crates/l/cobble-core)](https://opensource.org/licenses/MIT)
[![Pipeline](https://gitlab.com/stefan99353/hoellenpolizei-rs/badges/main/pipeline.svg)](https://gitlab.com/stefan99353/hoellenpolizei-rs/-/pipelines)

**A Discord Bot that tries to guess the programming language of messages and displays them with markup.**

## Usage

1. Create a Discord Appliction and copy the bot token (Google is your friend)
2. Join the bot to your server
3. Paste the token in the config file under `discord_token`
4. Copy the Server ID `guild_id` where the bot should reside and paste it in the config under `discord_guild_id`
5. [Configure](#configuration) the bot
6. Start the application

### Dependencies

- Python 3.7 and up (tested with 3.9.16)
  - Ensure `LD_LIBRARY_PATH` includes `$PYENV_ROOT/versions/<version>/lib` when using pyenv
- [Guesslang](guesslang.readthedocs.io) (tested with 2.2.1)

### Configuration

The bot expects a configuration file in the current directory by default. The path of the config file can be changed using the `CONFIG_FILE` environment variable.

A sample config file is provided [here](config/config.sample.toml).

All the values from the configuration file can be overridden with environment variables using the `BOT_` prefix.

### Container Image

Every release automatically builds a container image and pushes it to this repos registry [here](https://gitlab.com/stefan99353/hoellenpolizei-rs/container_registry).

```shell
docker run -d \
    --name hoellenpolizei \
    --env BOT_DISCORD_TOKEN="<DISCORD_TOKEN>" \
    --env BOT_DISCORD_GUILD_ID=<GUILD_ID> \
    registry.gitlab.com/stefan99353/hoellenpolizei-rs:latest
```

To use a config file and preserve the database/logs to the current directory:

```shell
# Config File: $PWD/config
# Logs: $PWD/logs/hoellenpolizei.log
# Database: $PWD/hoellenpolizei.db

docker run -d \
    --name hoellenpolizei \
    --volume $(pwd):/data \
    --env BOT_DISCORD_TOKEN="<DISCORD_TOKEN>" \
    --env BOT_DISCORD_GUILD_ID=<GUILD_ID> \
    --env BOT_LOG_FILE=true \
    registry.gitlab.com/stefan99353/hoellenpolizei-rs:latest
```

## Compiling

Compilation expects the python dynamic/shared library to be present.

- `python3-dev` on Debian
- Use `--enable-shared` when compiling python from source
- Install with `PYTHON_CONFIGURE_OPTS="--enable-shared"` using pyenv

```shell
cargo build --release
```

## Known Issues and Limitations

Due to the way [PyO3](https://crates.io/crates/pyo3) the bot does not handle `SIGINT` correctly. See this [issue](https://github.com/PyO3/pyo3/issues/2576) for more information.

Discord only allows messages with less than 2000 characters. This limitation also exists for this bot.

## License

`hoellenpolizei-rs` is distributed under the terms of the MIT license.

See [LICENSE](LICENSE) for details.
