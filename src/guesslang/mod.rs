mod language;

pub use language::Language;
use pyo3::Python;

#[instrument(name = "guess_language", level = "trace", skip_all, fields(lang))]
pub fn guess_language(source_code: &str) -> anyhow::Result<Language> {
    trace!("Aquire GIL lock");
    Python::with_gil(|py| {
        trace!("Import `guesslang` module");
        let guesslang = py.import("guesslang")?;
        trace!("Initializing guesslang");
        let guess = guesslang.call_method0("Guess")?;

        trace!("Calling `language_name`");
        let language_guess = guess.call_method1("language_name", (source_code,))?;

        trace!("Extracting result");
        let language: Option<String> = language_guess.extract()?;
        tracing::Span::current().record("lang", tracing::field::debug(&language));

        trace!("Parsing result");
        let language = language.map(Language::from).unwrap_or(Language::Other);

        Ok(language)
    })
}