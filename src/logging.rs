use crate::config::Config;
use file_rotate::compression::Compression;
use file_rotate::suffix::{AppendTimestamp, FileLimit};
use file_rotate::{ContentLimit, FileRotate};
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::prelude::*;
use tracing_subscriber::{fmt, reload};
use tracing_subscriber::{EnvFilter, Registry};

pub type LogReloadHandle = reload::Handle<EnvFilter, Registry>;

pub fn init(config: &Config) -> anyhow::Result<(LogReloadHandle, Option<WorkerGuard>)> {
    // Logs compatibility
    tracing_log::LogTracer::init()?;

    // Filter
    let (env_filter, reload_handle) = reload::Layer::new(
        EnvFilter::try_from_default_env()
            .unwrap_or_else(|_| EnvFilter::new(config.log_level.as_deref().unwrap_or("info"))),
    );

    // File
    let (file_layer, guard) = match config.log_file {
        true => {
            let writer = FileRotate::new(
                &config.log_file_path,
                AppendTimestamp::default(FileLimit::MaxFiles(config.log_max_file_count)),
                ContentLimit::BytesSurpassed(config.log_max_file_size),
                Compression::None,
                #[cfg(unix)]
                None,
            );

            let (writer, guard) = tracing_appender::non_blocking(writer);

            let layer = fmt::Layer::new()
                .with_writer(writer)
                .with_ansi(false)
                .event_format(fmt::format().json())
                .fmt_fields(fmt::format::JsonFields::new());

            (Some(layer), Some(guard))
        }
        false => (None, None),
    };

    // Stdout
    let stdout_layer = match config.log_stdout {
        true => Some(fmt::Layer::new()),
        false => None,
    };

    let registry = Registry::default()
        .with(env_filter)
        .with(stdout_layer)
        .with(file_layer);

    tracing::subscriber::set_global_default(registry)?;

    Ok((reload_handle, guard))
}
