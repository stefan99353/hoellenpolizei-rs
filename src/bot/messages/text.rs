use crate::bot::Hoellenpolizei;
use crate::guesslang::guess_language;
use serenity::model::channel::Message;
use serenity::prelude::*;
use serenity::utils::MessageBuilder;

#[instrument(name = "markup_text", skip_all, fields(lang))]
pub async fn markup(_bot: &Hoellenpolizei, msg: Message, ctx: &Context) -> anyhow::Result<()> {
    trace!("Guessing language of message");
    let content = msg.content.clone();
    let language = tokio::task::spawn_blocking(move || guess_language(&content)).await??;
    tracing::Span::current().record("lang", tracing::field::debug(language));

    // TODO: Format code

    trace!("Removing original message");
    msg.delete(&ctx.http).await?;

    trace!("Building response message");
    let mut message = MessageBuilder::new();
    message.push_bold_safe(&msg.author.name).push(" posted");

    if let Some(lang) = language.markdown_language() {
        message.push(" (").push_italic_safe(lang).push(")");
    }

    message
        .push_line("")
        .push_codeblock_safe(msg.content, language.markdown_language())
        .build();

    trace!("Sending formatted text message");
    msg.channel_id.say(&ctx.http, message).await?;

    Ok(())
}
