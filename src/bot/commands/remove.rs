use crate::bot::commands::HoellenpolizeiCommand;
use crate::bot::Hoellenpolizei;
use crate::db::EnabledChannel;
use serenity::model::application::interaction::application_command::ApplicationCommandInteraction;
use serenity::model::application::interaction::InteractionResponseType;
use serenity::prelude::*;

pub struct RemoveChannelCommand;

#[async_trait::async_trait]
impl HoellenpolizeiCommand for RemoveChannelCommand {
    const COMMAND: &'static str = "pozilei-remove";
    const DESCRIPTION: &'static str =
        "Removes this channel from the list of channels the bot to listens to";

    async fn process(
        hoellenpolizei: &Hoellenpolizei,
        command: &ApplicationCommandInteraction,
        ctx: &Context,
    ) -> anyhow::Result<()> {
        trace!("Getting channel information");
        let channel_id = command.channel_id.to_string();

        trace!("Removing channel from database");
        EnabledChannel::delete(channel_id, &hoellenpolizei.db).await?;

        trace!("Responding to the command");
        command
            .create_interaction_response(&ctx.http, |response| {
                response
                    .kind(InteractionResponseType::ChannelMessageWithSource)
                    .interaction_response_data(|message| {
                        message.content("Hussa, er muss dann mal weg!")
                    })
            })
            .await?;

        Ok(())
    }
}
